
envURL = 'web1.tunnlr.com:13082';

/*************************************************************************************************/

function fetchData() {

  var data = {};
  
  matches = google.contentmatch.getContentMatches();
  
  i=1;
  for (var match in matches) {
        
        for (var key in matches[match]) { 

            if (key == "subject") {
                data['email_subject'] = matches[match][key];
            }                                

            if (key == "recipient_cc_email" || key == "recipient_to_email") {
                data['email_'+i.toString()] = JSON.stringify({'email' : matches[match][key], 'key' : key});
                i++;
            }

            if (key == "sender_email") {
                data['email_'+i.toString()] = JSON.stringify({'email' : matches[match][key], 'key' : key});
                i++;
            }

            if (key == "message_id") {
                data['email_id'] = matches[match][key];
            }

            if (key == "email_body") {
                data['email_body'] = matches[match][key];
                emails_body = matches[match][key];
                emails_body = extractEmails(emails_body);
                for (var email in emails_body){
                  data['email_'+i.toString()] = JSON.stringify({'email' : emails_body[email], 'key' : key});
                  i++;
                }
            }
     
        }

  }
  data['num_emails'] = i-1;
  return(data);
};

/*************************************************************************************************/

function extractEmails (text){
  return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

/*************************************************************************************************/        

function init() {

var data = fetchData();
data = JSON.stringify(data);
localStorage.data = data;
searchContacts();

};

/*************************************************************************************************/

function searchContacts() {

    jQuery('#ask_menu').hide();
    jQuery('#load').show();

    data = JSON.parse(localStorage.data);
    postdata = gadgets.io.encodeValues(data);
    
    var params = {};
    url = 'http://' + envURL + '/gadget/gadget_contacts'
    params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.POST;
    params[gadgets.io.RequestParameters.POST_DATA]= postdata;
    params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.TEXT;
    params[gadgets.io.RequestParameters.AUTHORIZATION] = gadgets.io.AuthorizationType.SIGNED;

    gadgets.io.makeRequest(url, handleLoadResponse, params);
 };

/*************************************************************************************************/

function handleLoadResponse(obj) {
  var data = obj;
  jQuery('#content').html(data.text);
  size = jQuery('#content').parents('body').height();  
  resize = size + 10;  
  gadgets.window.adjustHeight(resize);  
};

/*************************************************************************************************/

function insertComment() {

    jQuery('#ask_insert').hide();
    jQuery('#load_insert').show();

    data = JSON.parse(localStorage.data);
    var select_val = jQuery('#select_object_target').val(),
        select_text = jQuery('#select_object_target option:selected').text().split(' - ')[0].toLowerCase()+'s';    
    data['object_name'] = select_text;
    data['object_id'] = select_val;               
    postdata = gadgets.io.encodeValues(data);

    var params = {};
    url = 'http://' + envURL + '/gadget/insert_mail_like_comment'
    params[gadgets.io.RequestParameters.METHOD] = gadgets.io.MethodType.POST;
    params[gadgets.io.RequestParameters.POST_DATA]= postdata;
    params[gadgets.io.RequestParameters.CONTENT_TYPE] = gadgets.io.ContentType.TEXT;
    params[gadgets.io.RequestParameters.AUTHORIZATION] = gadgets.io.AuthorizationType.SIGNED;

    gadgets.io.makeRequest(url, handleInsertResponse, params);
 };

/*************************************************************************************************/

function handleInsertResponse(obj) {
  var data = obj;
  jQuery('#insert_email_as_comment').html(data.text);
};

/*************************************************************************************************/

gadgets.util.registerOnLoadHandler(init);

/*************************************************************************************************/