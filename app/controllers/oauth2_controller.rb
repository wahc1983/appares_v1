# encoding: utf-8

class Oauth2Controller < ApplicationController
   include OauthConnection
   

  #-------------------------------------------------------------------------------------
  #
  #-------------------------------------------------------------------------------------
  public
  def index
    
    render(:text => params.inspect)
  end
  


  #-------------------------------------------------------------------------------------
  # authorize:  oauth v 1.0
  #-------------------------------------------------------------------------------------
  public
  def authorize_v1
    
    auth_url = nil
    request_token = nil
    
    # kind = "linkedin etc."
    # service: signup, ect
    session[:kind] = params[:kind] unless ( params[:kind].blank? )
    session[:service] = params[:service] unless ( params[:service].blank? )
    session[:business_file_id]  = params[:business_file_id] unless ( params[:business_file_id].blank? )
    if params[:auth_origin_url].to_s > ''
      session[:oauth_origin] = params[:auth_origin_url].gsub('__', '/')
    end
    
    use_ssl = request.ssl?
    @oauth_v1_connection = OauthV1Connection.new(session[:kind],session[:service], session[:business_file_id], use_ssl)

    if (session[:kind] == Eshop::K_SHOP_MAGENTO)
      eshop_pm = session[:eshop_params]['eshop']
      config_params = EshopMagento.get_config_params(eshop_pm)
      @oauth_v1_connection.config_params = config_params
    end

    if params[:oauth_verifier].blank?
      begin
        callback = url_for(:action => 'authorize_v1', :business_file_id => nil)
        auth_url, request_token = @oauth_v1_connection.get_auth_url(callback)
        session[:request_token] = request_token
        redirect_to(auth_url)
        return
      rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}<br/>auth_url=#{auth_url}<br/>request_token=#{request_token}"
        StaffNotifier.deliver_admin_message("Err in oauthflow/authorize_v1/get_auth_url", "exc = #{err}", 30)
        flash[:error] = "Votre identification a echoué"
        redirect_to( session[:oauth_origin] )
        return
      end
    else
      begin
        callback = url_for(:action => 'authorize_v1', :business_file_id => nil)
        access_token = @oauth_v1_connection.get_access_token(params[:oauth_verifier], session[:request_token])
        session[:oauth_flow] = { :access_token => access_token, :refresh_token => nil }
        rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}"
        StaffNotifier.deliver_admin_message("Err in oauthflow/authorize_v1/get_auth_url", "exc = #{err}", 30)
      end

      begin
        session[:business_file_id] = nil
        session[:service] = nil
        session[:kind] = nil

        redir_uri = @oauth_v1_connection.get_redirect_url_in_hash_after_auth
        redirect_to(redir_uri)
        return
        rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}"
        StaffNotifier.deliver_admin_message("Err in oauthflow/authorize_v1 1/get_redirect_url_in_hash_after_auth", "exc = #{err}", 30)
      end


    end
    rescue Exception => exc
          err = "Error: #{exc.message}<br/><br/>#{exc.backtrace.to_s}"
          StaffNotifier.deliver_admin_message("Error on Controllers/oauth2_controller/authorize_v1", err, 30)
    render :text => "Error"
    return
  end

  
  #-------------------------------------------------------------------------------------
  #
  #-------------------------------------------------------------------------------------
  public
  def authorize
    
    authorize_v2
  end
  
  #-------------------------------------------------------------------------------------
  #
  #-------------------------------------------------------------------------------------
  public
  def authorize_v2

    # error : dev.incwo.com/oauth2/authorize_v2?code=4/zwerXuvZ9H5bBMjXwof-GPb0eXfGaL-7rQV_ASpgc-E.Isuuem89AiIVoiIBeO6P2m9AaNfimgI
    
    if params[:from] == 'gapps'
      # https://www.incwo.com/oauth2/authorize?auth_origin_url=__site__compte&kind=google&service=signup
      redirect_to(params.merge({
        :auth_origin_url => "__site__compte", 
        :kind => 'google_gadget', 
        :service => 'signup', 
        :from => nil
      }))
      return
    end
    
    debug = []
    debug << "oauth2/authorize"
    debug << params.inspect
    
    # kind = "google",etc
    # service: drive, calendar, contacts,etc
    session[:kind] = params[:kind] unless ( params[:kind].blank? )
    session[:service] = params[:service] unless ( params[:service].blank? )
    session[:business_file_id] = params[:business_file_id] unless ( params[:business_file_id].blank? )
    if params[:auth_origin_url].to_s > ''
      val = params[:auth_origin_url] #{ }"__account__gadget_setup"
      # val = val.to_s.gsub('__account__gadget_setup', '__account__login')
      session[:oauth_origin] = val.gsub('__', '/')
    end

    debug << "session[:oauth_origin]=#{session[:oauth_origin]}"

    if session[:kind].blank? || session[:service].blank?
      debug << "rebuild"
      StaffNotifier.deliver_admin_message("oauth rebuild auth2", "#{session[:kind]}>#{session[:service]}<br/><br/>#{params.inspect}", 30)

      # we have a problem here... so try to build kind & service from referrer... 
      str = ((request.env['HTTP_REFERER'].to_s rescue '').split('?')[1] rescue '')
      if str.to_s.size > 2
        pars = str.split('&')
        for par in pars
          pa = par.split('=')
          if pa[0] == 'kind' && session[:kind].blank?
            session[:kind] = pa[1]
          elsif pa[0] == 'service' && session[:service].blank?
            session[:service] = pa[1]
          end
        end
      end
    else
      debug << "not rebuild"
    end
    
    use_ssl = request.ssl?
    @oauth_flow = OauthFlow.new(session[:kind], session[:service], session[:business_file_id], use_ssl)

    debug << "kind=#{session[:kind]}"
    debug << "service=#{session[:service]}"
    debug << "auth_origin_url=#{params[:auth_origin_url]}"
    debug << "oauth_origin=#{params[:oauth_origin]}"
    debug << "use_ssl=#{use_ssl}"
    debug << "oauth_flow=#{@oauth_flow.inspect}"

    if params[:code].blank?
      # Get first token and redirect the user to grant access to our app
      begin
        debug << "no code"
        callback = url_for(:action => 'authorize_v2', :business_file_id => nil)
        auth_url = @oauth_flow.get_auth_url(callback)
        debug << "callback=#{callback.inspect}"
        debug << "auth_url=#{auth_url.inspect}"
        BusinessFile.find(30).keep_trace(nil, debug.join('<br/>'), 'social_login/debug1', 7)
        redirect_to(auth_url)
        return
      rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}"
        debug << "err1=#{err}"
        StaffNotifier.deliver_admin_message("Err in oauthflow/authorize_v2/get_auth_url", "exc = #{err}", 30)
      end

    else
      begin
        debug << "code"
        callback = url_for(:action => 'authorize_v2', :business_file_id => nil)
        access_token, refresh_token = @oauth_flow.get_first_access_token( params[:code], callback )
        session[:oauth_flow] = { :access_token => access_token, :refresh_token => refresh_token }
        debug << "callback=#{callback.inspect}"
        debug << "access_token=#{access_token.inspect}"
        debug << "refresh_token=#{refresh_token.inspect}"
        #access_token_obj = client.auth_code.get_token( params[:code], :redirect_uri => redirect_uri, :token_method => :post)

        # for contacts API we could need to specify the api version
        # To specify a version number, use the GData-Version HTTP header:
        #  GData-Version: 3.0 ---> ....  :redirect_uri => redirect_uri, :headers => {'GData-Version' => '3.0'}
        # Alternatively, if you can't set HTTP headers, you can specify v=3.0 as a query parameter in the URL. 
        # The HTTP header is preferred where possible.
        # https://developers.google.com/google-apps/contacts/v3/index#authorizing_requests_to_the_+wzxhzdk8+_service
      rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}"
        debug << "err2=#{err}"
        StaffNotifier.deliver_admin_message("in Err in oauthflow/authorize_v2/get_first_access_token", "exc = #{err}", 30)
        access_token = nil
        refresh_token = nil
      end

      begin
        session[:business_file_id] = nil
        session[:service] = nil
        session[:kind] = nil

        redir_uri = @oauth_flow.get_redirect_url_in_hash_after_auth
        debug << "redir_uri=#{redir_uri.inspect}"
        BusinessFile.find(30).keep_trace(nil, debug.join('<br/>'), 'social_login/debug2', 7)
        redirect_to(redir_uri)
        return
      rescue Exception => exc
        err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>" + 
                    "#{request.inspect[0..25000] rescue '?'}<br/><br/>#{@config_params.inspect}--#{@service.inspect}--#{@kind.inspect}"
        debug << "err3=#{err}"
        StaffNotifier.deliver_admin_message("Err in oauthflow/authorize_v2/get_redirect_url_in_hash_after_auth", "exc = #{err}", 30)
      end
    end

    BusinessFile.find(30).keep_trace(nil, debug.join('<br/>'), 'social_login/debug3', 7)

  rescue Exception => exc
    err = "#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>#{params.inspect rescue '?'}<br/><br/>#{request.inspect[0..25000] rescue '?'}"
    StaffNotifier.deliver_admin_message("Err in oauthflow/authorize2", "exc = #{err}", 30)
  end


end