# encoding: utf-8

require 'oauth2'

module OauthConnection

end


class OauthFlow

  #-------------------------------------------------------------------------------------
  # initialize
  #-------------------------------------------------------------------------------------
  def initialize( kind, in_service, business_file_id, use_ssl)
    @kind = kind
    @service = in_service
    @business_file_id = business_file_id
    @use_ssl = use_ssl
    @config_params = nil
    get_config_params
  end


  #-------------------------------------------------------------------------------------
  # get_config_params gets the*.yml file to load the client_id,client_secret,scopes and callback urls
  #-------------------------------------------------------------------------------------
  private
  def get_config_params

    env = ENV['RAILS_ENV'] || RAILS_ENV

    yam_file_name = @kind == 'google_gadget' ? 'google' : @kind

    if ( File.exist?(Rails.root.to_s + "/config/#{yam_file_name}.yml") )
      @config_params = YAML.load_file("#{Rails.root.to_s}/config/#{yam_file_name}.yml")[env]
    else
      raise "#{yam_file_name}.yml not found"
    end

  end

  #-------------------------------------------------------------------------------------
  # get_redirect_uri redirect uri registered on the webservice (callback):
  # like the google API CONSOLE https://code.google.com/apis/console/
  #-------------------------------------------------------------------------------------
  private
  def get_redirect_uri
    redirect_uri = url_for(:action => 'authorize', :business_file_id => nil)
    return(redirect_uri)
  end

  #-------------------------------------------------------------------------------------
  # get_token_method :post" or :get
  #-------------------------------------------------------------------------------------
  private
  def get_token_method
    if ( @config_params["#{@service}"]['token_method'].blank? )
      return(:post)
    else
      @config_params["#{@service}"]['token_method'].to_sym
    end
  end

  #-------------------------------------------------------------------------------------
  # get_parse :query or nil
  #-------------------------------------------------------------------------------------
  private
  def get_parse
    if ( @config_params["#{@service}"]['parse'].blank? )
      return(nil)
    else
      @config_params["#{@service}"]['parse'].to_sym
    end
  end

  #-------------------------------------------------------------------------------------
  # get_scopes
  #-------------------------------------------------------------------------------------
  private
  def get_scopes
    @config_params["#{@service}"]['scopes']
  end

  #-------------------------------------------------------------------------------------
  # get_get_approval_prompt
  #-------------------------------------------------------------------------------------
  private
  def get_approval_prompt
    @config_params["#{@service}"]['approval_prompt']
  end

  #-------------------------------------------------------------------------------------
  # get_access_type
  #-------------------------------------------------------------------------------------
  private
  def get_access_type
    @config_params["#{@service}"]['access_type']
  end

  
  #-------------------------------------------------------------------------------------
  # get_client: gets oauth client
  #-------------------------------------------------------------------------------------
  private
  def get_client( in_options = { :token_method => :post })

    get_config_params
    if ( @use_ssl )
      client_id = @config_params['ssl']['client_id']
      client_secret = @config_params['ssl']['client_secret']
        #StaffNotifier.deliver_admin_message("in oauth_connection module/YAMPSSL", "client_id = #{client_id}<br/>client_secret = #{client_secret}", 30)
    else
      client_id = @config_params['http']['client_id']
      client_secret = @config_params['http']['client_secret']
        #StaffNotifier.deliver_admin_message("in oauth_connection module/YAMPP", "client_id = #{client_id}<br/>client_secret = #{client_secret}", 30)
    end

    client = OAuth2::Client.new( client_id,
                                  client_secret,
                                  :site => @config_params["site"],
                                  :token_url => @config_params["token_url"],
                                  :authorize_url => @config_params["authorize_url"],
                                  :token_method => in_options[:token_method])
    raise "error getting client #{@config_params.inspect} #{@kind}" unless (client)

    if @config_params["site"].to_s == '' || @config_params["token_url"].to_s == '' || @config_params["authorize_url"].to_s == ''
      StaffNotifier.deliver_admin_message("missing data in config file", "@config_params = #{@config_params.inspect}", 30)
    end
    
    return(client)

  rescue Exception => exc
    err = "Err in oauth_connection.get_client : #{exc.message}<br/><br/>#{exc.backtrace.to_s}"
    StaffNotifier.deliver_admin_message("in oauth_connection module2", "exc = #{err}", 30)
    return nil
  end

  #-------------------------------------------------------------------------------------
  # update_user_tokens: update the user token values
  #-------------------------------------------------------------------------------------
  private
  def update_object_tokens(the_object, in_access_token = "", in_refresh_token = "")

    #if( @kind == "google")
    # update access token
    unless (in_access_token.blank?)
      if( @kind == "google" && @service != "drive")
        case @service
        when "calendar"
          the_object.google_calendar_access_token = in_access_token
        when "contacts"
          the_object.google_contacts_access_token = in_access_token
        end
      elsif ( @kind == "google_gadget" )
          the_object.google_gadget_access_token = in_access_token
      else
        the_object.access_token = in_access_token
      end
    end

    #update refresh token
    unless (in_refresh_token.blank?)
      if( @kind == "google" && @service != "drive")
        case @service
        when "calendar"
          the_object.google_calendar_refresh_token = in_refresh_token
        when "contacts"
          the_object.google_contacts_refresh_token = in_refresh_token
        end      
      elsif ( @kind == "google_gadget" )
          the_object.google_gadget_refresh_token = in_refresh_token
      else
        the_object.refresh_token = in_refresh_token
      end
    end

    #else # other services

    #end
    the_object.save_without_validation
  end

  #-------------------------------------------------------------------------------------
  # get_client: gets oauth client
  #-------------------------------------------------------------------------------------
  #def get_authorization_access_token(code)
  #  client = get_client
  #  redirect_uri = get_redirect_uri
  #  client.auth_code.get_token( code, :redirect_uri => redirect_uri, :token_method => :post)
  #end


  #-------------------------------------------------------------------------------------
  # get_auth_url gets oauth client
  #-------------------------------------------------------------------------------------
  public
  def get_auth_url(callback)
    get_config_params
    client = get_client({:token_method => get_token_method})
    redirect_uri = callback
    scopes = get_scopes
    approval_prompt = get_approval_prompt
    access_type = get_access_type
    token_method = get_token_method
    auth_url = client.auth_code.authorize_url(:redirect_uri => redirect_uri,
      :access_type => "offline",
      :scope => "#{scopes}",
      :approval_prompt => "#{approval_prompt}",
      :token_method => "#{token_method}" )
    return(auth_url)
  end


  #-------------------------------------------------------------------------------------
  # get_first_access_token : get the access firts access token after the auth
  #-------------------------------------------------------------------------------------
  def get_first_access_token(in_code = "", callback = "")
    
    client = get_client({:token_method => get_token_method})
    redirect_uri = callback

    access_token_obj = client.auth_code.get_token( in_code, :redirect_uri => redirect_uri, :parse => get_parse)

    msg_soc = "callbk = #{callback.inspect}<br/><br/>client=#{client.inspect}<br/><br/>cd=#{in_code.inspect}<br/><br/>tk=#{access_token_obj.inspect}"
    BusinessFile.find(30).keep_trace(nil, msg_soc, 'social_login/get_first_access_token', 7)

    return(["#{access_token_obj.token}", "#{access_token_obj.refresh_token}"])
  end

  #-------------------------------------------------------------------------------------
  # get_access_token : get the access token or refresh it if it is expired
  # dropbox accounts the_object is the instance of @dropbox_account
  # google contacts and calendar the_object is the instance of user
  #-------------------------------------------------------------------------------------
  public
  def get_access_token(the_object, in_access_token = nil, in_refresh_token = nil)

    if (in_access_token.blank? )
      if ( @kind == "google" && @service != "drive" )
        access_token = case @service
        when "calendar" then the_object.google_calendar_access_token
        when "contacts" then the_object.google_contacts_access_token
        when "tasks" then the_object.google_tasks_access_token
        else nil
        end
        elsif ( @kind == "google_gadget" )
        access_token = the_object.google_gadget_access_token
        else
        access_token = the_object.access_token
      end

      client = get_client
      if ( @kind == "google" && @service != "drive" )
        refresh_token = case @service
        when "calendar" then the_object.google_calendar_refresh_token
        when "contacts" then the_object.google_contacts_refresh_token
        when "tasks" then the_object.google_tasks_refresh_token
        else nil
        end
      elsif ( @kind == "google_gadget" )
        refresh_token = the_object.google_gadget_refresh_token        
      else
        refresh_token =  the_object.refresh_token
      end
      opts = (refresh_token.blank?) ?  {} : {:refresh_token => refresh_token}
    else
      client = get_client(:token_method => get_token_method)
      opts = (in_refresh_token.blank?) ?  {} : {:refresh_token => in_refresh_token}
      access_token_obj = OAuth2::AccessToken.new(client, in_access_token, opts )
      return access_token_obj
    end

    access_token_obj = OAuth2::AccessToken.new(client, access_token, opts )

    begin
      if (!refresh_token.blank?)
        access_token_obj = access_token_obj.refresh! 
      end
      update_object_tokens(the_object, "#{access_token_obj.token}", "#{access_token_obj.refresh_token}" )
    rescue Exception => exc
      err = "#{@kind}/a=#{in_access_token}#{in_refresh_token}<br/>#{the_object.class}(#{the_object.id rescue ''})<br/>#{exc.message}<br/>#{exc.backtrace.to_s}"
      StaffNotifier.deliver_admin_message("Err in OAUTH/REFRESH", "exc = #{err}", 30)
    end
    
    return(access_token_obj)
  end

  #-------------------------------------------------------------------------------------
  # get_redirect_url_in_hash_after_auth
  #-------------------------------------------------------------------------------------
  public
  def get_redirect_url_in_hash_after_auth

    pr = @config_params["#{@service}"]
    the_controller = pr['redirect_after_auth_controller']
    the_action = pr['redirect_after_auth_action']
    url = { :controller => the_controller,
            :action => the_action,
            :business_file_id => @business_file_id,
            :kind => @kind,
            :service => @service
      }
    return(url)
    
    rescue Exception => exc
      err = "service=#{@service}//c=#{the_controller}//a=#{the_action}//url=#{url.inspect}//kind=#{@kind}<br/>file=#{@business_file_id}"
      err += "<br/>#{exc.message}<br/><br/>#{exc.backtrace.to_s}<br/><br/>@config_params=#{@config_params.inspect}<br/>pr=#{pr.inspect}"
      StaffNotifier.deliver_admin_message("ERR in get_redirect_url_in_hash_after_auth", "#{err}", 30)
      return nil
  end

end
